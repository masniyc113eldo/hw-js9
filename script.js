function displayList(array, parent = document.body) {
    const list = document.createElement("ul");

    array.map(item => {
        const listItem = document.createElement("li");

        if (Array.isArray(item)) {
            const nestedList = document.createElement("ul");
            listItem.appendChild(nestedList);
            displayList(item, nestedList);
        } else {
            listItem.textContent = item;
        }

        list.appendChild(listItem);
    });

    parent.appendChild(list);
}

const array = ["hello", "world", "Kiev", "Kharkiv", ["Borispol", "Irpin"], "Odessa", "Lviv"];
displayList(array);

let count = 3;

function countdown() {
    if (count >= 1) {
        console.log("Page will be cleared", count, "sec.");
        count--;
        setTimeout(countdown, 1000);
    } else {
        document.body.innerText = "";
        console.log("Page has been cleared.");
    }
}

countdown();
